from tkinter import *
import random
import sys
import time
import threading

def copy_tab_map(tab_forest):
    """
    Fonction permettant de copier la liste contenant les états de chaques cases
    """
    tmp = list()
    for lists in tab_forest:
        new_list = list(lists)
        tmp.append(new_list)
    return tmp

def cinder_to_empty(tab_forest):
    """
    Fonction permettant de passer les cendre à vide
    """

    tmp = copy_tab_map(tab_forest)
    
    for index_x in range(0, len(tmp)) :
        for index_y in range(0, len(tmp[1])) :
            if tmp[index_x][index_y] == "cendre" :
                tmp[index_x][index_y] = "vide"
    
    return tmp

def fire_to_cinder(tab_forest):
    """
    Fonction permettant de passer les feux en cendres
    """

    tmp = copy_tab_map(tab_forest)
    
    for index_x in range(0, len(tmp)) :
        for index_y in range(0, len(tmp[1])) :
            if tmp[index_x][index_y] == "feu" :
                tmp[index_x][index_y] = "cendre"

    return tmp

def tree_to_fire_first_rule(tab_forest):
    """
    Fonction permettant de passer les arbres en feux avec une probabilité de 100%
    """
    ##On fait d'abord passer les cendre en vide, puis les feux en cendre

    tmp = copy_tab_map(tab_forest)

    ##Puis on utilise les cendres (anciennement feu avant de passer dans fire_to_cinder) pour passer les voisin en feu
    for index_x in range(0, len(tmp)) :
        for index_y in range(0, len(tmp[1])) :
            if tab_forest[index_x][index_y] == "cendre" :
                if index_x - 1 >= 0 :
                    if tab_forest[index_x - 1][index_y] == "arbre" :
                        tmp[index_x - 1][index_y] = "feu"
                if index_x + 1 < len(tmp) :
                    if tab_forest[index_x + 1][index_y] == "arbre" : 
                        tmp[index_x + 1][index_y] = "feu"
                if index_y - 1 >= 0 :
                    if tab_forest[index_x][index_y - 1] == "arbre" :
                        tmp[index_x][index_y - 1] = "feu"
                if index_y + 1 < len(tmp[1]) :
                    if tab_forest[index_x][index_y + 1] == "arbre" :
                        tmp[index_x][index_y + 1] = "feu"
    
    return tmp

def tree_to_fire_second_rule(tab_forest):
    """
    Fonction permettant de passer les arbres en feux selon une équation de probabilité
    """

    tmp = copy_tab_map(tab_forest)

    for index_x in range(len(tmp)):
        for index_y in range(len(tmp[1])):
            if tmp[index_x][index_y] == "arbre" :
                k = 0
                if index_x - 1 >= 0 :
                    if tab_forest[index_x - 1][index_y] == "cendre" : #On teste sur cendre car on vient de passer ceux en feux à cendre
                        k = k + 1
                if index_x + 1 < len(tmp) :
                    if tab_forest[index_x + 1][index_y] == "cendre" : 
                        k = k + 1
                if index_y - 1 >= 0 :
                    if tab_forest[index_x][index_y - 1] == "cendre" :
                         k = k + 1
                if index_y + 1 < len(tmp[1]) :
                    if tab_forest[index_x][index_y + 1] == "cendre" :
                        k = k + 1
                res = random.random()
                if res < 1-(1/(k+1)):
                    tmp[index_x][index_y] = "feu"

    return tmp

def isparsable(value):
    """
    Fonction indiquant si une valeur est parsable ou non
    """
    try:
        float(value)
        return True
    except ValueError:
        return False

def parse_param(argv):
    """
    Fonction permettant de parser les paramètres ou de mettre ceux par défaut selon les indications de l'utilisateur
    """
    result = list()

    #On initialise une liste avec des valeurs prédéfinites
    result.append('') 
    result.append('') 
    result.append('') 
    result.append('') 
    result.append('')
    
    if len(argv) > 5 :
        sys.exit(1)
    
    #On met à jour la liste initialisé plus haut si les paramètres sont parsable
    for param in argv:
        if "rows=" in param:
            if isparsable(param.split("=")[1]):
                result[0] = int(param.split("=")[1])
        if "cols=" in param:
            if isparsable(param.split("=")[1]):
                result[1] = int(param.split("=")[1])
        if "cell_size=" in param:
            if isparsable(param.split("=")[1]):
                result[2] = int(param.split("=")[1])
        if "afforestation=" in param:
            if isparsable(param.split("=")[1]):
                result[3] = float(param.split("=")[1])
        if "anim_time=" in param:
            if isparsable(param.split("=")[1]):
                result[4] = float(param.split("=")[1])
                
    #On assigne des valeurs par défaut si elle n'ont pas été modifié par la moulinette
    for index in range(0, 5):
        if result[index] == '':
            if index == 0 or index == 1:
                result[index] = 10
            if index == 2:
                result[index] = 20
            if index == 3:
                result[index] = .6
            if index == 4:
                result[index] = 2
    
    if result[0] * result[2] >= 1200 or result[1] * result[2] >= 900:
        print("Paramètres des carré trop grand pour pouvoir être affiché")
        sys.exit(2)

    return result

def initialize_tab_simulation(param):
    """
    Fonction permettant d'initialiser la liste contenant tout les états de manière aléatoire
    """
    new_list = list()

    #On initialise un tableau aléatoirement selon les paramètres passé par l'utilisateur
    for index_a in range(0, param[0]):
        other_list = list()
        for index_b in range(0, param[1]):
            rand = random.randint(1, 10)
            if rand <= param[3] * 10: 
                etat = "arbre"
            else:
                etat = "vide"
            other_list.append(etat)
        new_list.append(other_list)

    return new_list

def initialize_canvas(canvas, param, tab_forest):
    """
    Méthode permettant d'initialisé le canvas
    """
    #On ajoute les carré 1 à 1
    x_pos = 0
    for index_x in tab_forest:
        y_pos = 0
        for etat in index_x:
            if etat == "vide":
                canvas.create_rectangle(x_pos, y_pos, x_pos + param[2], y_pos + param[2], fill="grey")
            if etat == "arbre":
                canvas.create_rectangle(x_pos, y_pos, x_pos + param[2], y_pos + param[2], fill="green")
            y_pos = y_pos + param[2]
        x_pos = x_pos + param[2]

def reset_tab_simulation(tab_forest):
    """
    Méthode permettant de reset tab_forest sans modifié la référence
    """
    for index_x in range(len(tab_forest)):
        for index_y in range(len(tab_forest[1])):
            rand = random.randint(1, 10)
            if rand <= param[3] * 10: 
                etat = "arbre"
            else:
                etat = "vide"
            tab_forest[index_x][index_y] = etat

def retrieve_index_carre(x, y, param):
    """
    Fonction permettant de retrouver l'index d'un carré dans tab_forest depuis les coordonnées d'un click
    """
    result = list()

    if x%param != 0 and y%param !=0:
        result.append(int(x/param))
        result.append(int(y/param))
    else:
        result.append(-1)
        result.append(-1)

    return result

def set_carre(event, param, tab_forest):
    """
    Méthode utilisé lors d'un clic sur l'un des carré
    """
    index_carre = retrieve_index_carre(event.x, event.y, param[2])
    if tab_forest[index_carre[0]][index_carre[1]] == "arbre" :
        tab_forest[index_carre[0]][index_carre[1]] = "feu"
        canvas.create_rectangle(index_carre[0]*param[2], index_carre[1]*param[2], index_carre[0]*param[2] + param[2], index_carre[1]*param[2] + param[2], fill="red")
    elif tab_forest[index_carre[0]][index_carre[1]] == "feu" :
        tab_forest[index_carre[0]][index_carre[1]] = "arbre"
        canvas.create_rectangle(index_carre[0]*param[2], index_carre[1]*param[2], index_carre[0]*param[2] + param[2], index_carre[1]*param[2] + param[2], fill="green")

def still_fire(tab_forest):
    """
    Fonction permettant de savoir s'il reste au moin 1 feu présent
    """
    for index_x in tab_forest:
        for etat in index_x:
            if etat == "feu":
                return True
    
    return False

def still_cinder(tab_forest):
    """
    Fonction permettant de savoir s'il reste au moin 1 cendre
    """
    for index_x in tab_forest:
        for etat in index_x:
            if etat == "cendre":
                return True
    
    return False

def update_canvas(canvas, tab_forest_before, tab_forest_after, param):
    """
    Méthode permettant de mettre à jour le canvas
    """
    for index_x in range(len(tab_forest_before)):
        for index_y in range(len(tab_forest_before[0])):
            if tab_forest_before[index_x][index_y] != tab_forest_after[index_x][index_y]:
                if tab_forest_after[index_x][index_y] == "feu":
                    canvas.create_rectangle(index_x * param[2], index_y * param[2], index_x * param[2] + param[2], index_y * param[2] + param[2], fill="red")
                if tab_forest_after[index_x][index_y] == "vide":
                    canvas.create_rectangle(index_x * param[2], index_y * param[2], index_x * param[2] + param[2], index_y * param[2] + param[2], fill="grey")
                if tab_forest_after[index_x][index_y] == "cendre":
                    canvas.create_rectangle(index_x * param[2], index_y * param[2], index_x * param[2] + param[2], index_y * param[2] + param[2], fill="#525252")
                if tab_forest_after[index_x][index_y] == "arbre":    
                    canvas.create_rectangle(index_x * param[2], index_y * param[2], index_x * param[2] + param[2], index_y * param[2] + param[2], fill="green")


def launch_simulation(tab_forest, param, canvas, state_thread):
    """
    Fonction effectuant la simulation
    """
    while still_fire(tab_forest) or still_cinder(tab_forest):
        if state_thread[0] : #Si pause on fait rien
            continue
        else:
            tmp = cinder_to_empty(tab_forest)
            tmp = fire_to_cinder(tmp)
            if state_thread[2] == True:
                tmp = tree_to_fire_second_rule(tmp)
            else:
                tmp = tree_to_fire_first_rule(tmp)
            update_canvas(canvas, tab_forest, tmp, param)
            ##On recopie sans changer la référence
            for index_x in range(len(tmp)):
                for index_y in range(len(tmp[1])):
                    tab_forest[index_x][index_y] = tmp[index_x][index_y]
            time.sleep(param[4]) 
    
    state_thread[1] = False #On remet cette valeur à False pour relancer un thread plus tard
    return None

if __name__ == '__main__' :
    
    param = parse_param(sys.argv)
    tab_forest = initialize_tab_simulation(param)

    master = Tk()
    master.title("Feu de forêt")

    master.rowconfigure(1, weight=1)
    master.columnconfigure(0, weight=1)
    master.columnconfigure(1, weight=1)
    master.columnconfigure(2, weight=1)
    master.columnconfigure(3, weight=1)

    canvas = Canvas(master, 
                    width=param[0]*param[2], 
                    height=param[1]*param[2])
    canvas.grid(row=1, columnspan=4, padx=10, pady=10, sticky='nsew')
    initialize_canvas(canvas, param, tab_forest)


    def click_callback_canvas(event, param=param, tab=tab_forest):
        set_carre(event, param, tab)

    canvas.bind('<Button-1>', click_callback_canvas)
    canvas.bind('<Button-3>', click_callback_canvas)

    state_thread = []
    state_thread.append(True) #Première valeur pour la pause
    state_thread.append(False) #Deuxième valeur si un thread est déjà en course
    state_thread.append(False) #La troisème correspond aux règles de transition de arbre à feu

    def click_callback_start():
        if not state_thread[1] : #Si un thread n'est pas déjà lancé
            state_thread[0] = False
            state_thread[1] = True
            th = threading.Thread(target=launch_simulation, args=(tab_forest,param,canvas,state_thread))
            th.start()

    btn_start_simu = Button(master,
                             text='Start simulation',
                             command=click_callback_start,
                             bg="#2980b9",
                             activebackground="#3498db")
    btn_start_simu.grid(row=2, column=1)

    btn_quit = Button(master,
                      text="Exit",
                      command=master.quit,
                      bg="#c0392b",
                      activebackground="#95a5a6",
                      fg="#2c3e50",
                      activeforeground="#2c3e50")
    btn_quit.grid(row=2, column=3)

    def click_callback_stop():
        if state_thread[0] == False:
            state_thread[0] = True
        else:
            state_thread[0] = False

    btn_pause = Button(master,
                      text="Pause",
                      command=click_callback_stop,
                      bg="#2980b9",
                      activebackground="#3498db")
    btn_pause.grid(row=2, column=2)

    def click_callback_reset():
        if state_thread[0] or not state_thread[1]:
            reset_tab_simulation(tab_forest)
            initialize_canvas(canvas, param, tab_forest)


    btn_reset = Button(master,
                      text="Reset",
                      command=click_callback_reset,
                      bg="#2980b9",
                      activebackground="#3498db")
    btn_reset.grid(row=2, column=0)

    def click_callback_checkbox():
        if state_thread[2] == False:
            state_thread[2] = True
        else:
            state_thread[2] = False

    checkbox_transition = Checkbutton(master,
                                      text = "Transition calcul",
                                      command=click_callback_checkbox)
    checkbox_transition.grid(row=0, columnspan=4)

    master.resizable(width=False, height=False)
    master.mainloop()