import unittest
import random
from app_fireforest import cinder_to_empty, fire_to_cinder, tree_to_fire, copy_tab_map, parse_param, isparsable, retrieve_index_carre, still_fire, still_cinder

class TestFireForest(unittest.TestCase):

    def setUp(self):
        """
        Création d'un tableau 2D contennant les états de chaques cases de la simulation
        """
        a = list()
        for index_a in range(1, 10):
            b = list()
            for index_b in range(1, 10):
                rand = random.randint(1, 10)
                if rand >= 5: ## Probabilité de 50% que la case soit un arbre
                    etat = "arbre"
                elif rand >= 2: ##Probabilité de 30% que la case soit vide
                    etat = "vide"
                else: ##Probabilité de 20% que la case soit feu
                    etat = "feu"
                b.append(etat)
            a.append(b)
        self.cases = a
    
    def test_copy_tab_map(self):
        """
        Test pour la fonction copy_tab_map
        """

        tmp = copy_tab_map(self.cases)

        self.assertEqual(tmp, self.cases)

    def test_cinder_to_empty(self):
        """
        Test pour la fonction cider_to_empty
        """
        #On prend le nombre de cendre et de vide avant de passer dans cinder_to_empty
        cinder_before = 0
        empty_before = 0
        for tab_x in self.cases :
            for etat_y in tab_x :
                if etat_y == "cendre" :
                    cinder_before = cinder_before + 1
                if etat_y == "vide" :
                    empty_before = empty_before + 1

        test = cinder_to_empty(self.cases)

        #On reprend le nombre de vide qui doit être égal à cendre_before + empty_before 
        cinder_after = 0
        empty_after = 0 
        for tab_x in test :
            for etat_y in tab_x :
                if etat_y == "vide" :
                    empty_after = empty_after + 1 
                if etat_y == "cendre" :
                    cinder_after = cinder_after + 1 

        #Puis on compare pour vérifier si la fonction cinder_to_empty a bien fonctionné
        self.assertEqual(empty_after, (empty_before + cinder_before))
        self.assertEqual(cinder_after, 0)

    def test_fire_to_cinder(self):
        """
        Test pour la fonction fire_to_cinder
        """
        #On prend nombre de cendre ainsi que de feu avant de passer dans fire_to_cinder
        fire_before = 0
        cinder_before = 0
        for tab_x in self.cases :
            for etat in tab_x :
                if etat == "feu" :
                    fire_before = fire_before + 1
                if etat == "cendre" :
                    cinder_before = cinder_before + 1
        
        test = fire_to_cinder(self.cases)

        #On reprend le nombre de cendre qui doit être égal à cendre_before + fire_before 
        cinder_after = 0
        fire_after = 0
        for tab_x in test :
            for etat in tab_x :
                if etat == "feu" :
                    fire_after = fire_after + 1
                if etat == "cendre" :
                    cinder_after = cinder_after + 1

        self.assertEqual(fire_after, 0)
        self.assertEqual(cinder_after, (cinder_before + fire_before))

    def test_tree_to_fire(self):
        """
        Test pour la fonction tree_to_fire
        """
        #On prend le nombre de feu ainsi que le nombres d'arbres voisins d'un feu
        fire_before = 0
        tree_before = 0

        #On crée un tableau de boolean afin de compter qu'une seule fois chaque case
        tab_boolean = list()
        for i in range(0, len(self.cases)):
            new_list = list()
            for y in range(0, len(self.cases[1])):
                new_list.append(False)
            tab_boolean.append(new_list)
                
        
        for index_x in range(0, len(self.cases)) :
            for index_y in range(0, len(self.cases[1])) :
                if self.cases[index_x][index_y] == "feu" :
                    fire_before = fire_before + 1
                    if index_x - 1 >= 0 :
                        if self.cases[index_x - 1][index_y] == "arbre" and tab_boolean[index_x - 1][index_y] is False:
                            tree_before = tree_before + 1
                            tab_boolean[index_x - 1][index_y] = True
                    if index_x + 1 < len(self.cases) :
                        if self.cases[index_x + 1][index_y] == "arbre" and tab_boolean[index_x + 1][index_y] is False: 
                            tree_before = tree_before + 1
                            tab_boolean[index_x + 1][index_y] = True
                    if index_y - 1 >= 0 :
                        if self.cases[index_x][index_y - 1] == "arbre" and tab_boolean[index_x][index_y - 1] is False:
                            tree_before = tree_before + 1
                            tab_boolean[index_x][index_y - 1] = True
                    if index_y + 1 < len(self.cases[1]) :
                        if self.cases[index_x][index_y + 1] == "arbre" and tab_boolean[index_x][index_y + 1] is False:
                            tree_before = tree_before + 1
                            tab_boolean[index_x][index_y + 1] = True
    
        ##On passe d'abord dans cinder_to_empty puis fire_to_cinder car tree_to_fire utilise les cendres restantes
        tmp = cinder_to_empty(self.cases)
        tmp = fire_to_cinder(tmp)

        test = tree_to_fire(tmp)

        #On test que tout les anciens arbres sont passé en feu
        fire_after = 0

        for tab_x in test :
            for etat in tab_x :
                if etat == "feu" :
                    fire_after = fire_after + 1

        self.assertEqual(fire_after, tree_before)

    def test_parse_param(self):
        """
        Test permettant la gestion des paramètres lors de l'exécution
        """
        ##On initialise un tableau simulé contenant les paramètres
        my_argv = list()

        rows = random.randint(2, 20)
        cols = random.randint(2, 20)
        cell_size = random.randint(25, 50)
        afforestation = random.random()

        my_argv.append("-rows=%d" %(rows))  
        my_argv.append("-cols=%d" %(cols))
        my_argv.append("-cell_size=%d" %(cell_size))  
        my_argv.append("-afforestation=%f" %(afforestation)) 
        my_argv.append("-anim_time=%s" %("..."))

        result = parse_param(my_argv)

        self.assertEqual(result[0], rows)
        self.assertEqual(result[1], cols)
        self.assertEqual(result[2], cell_size)
        self.assertEqual(result[3], round(afforestation, 6))
        self.assertEqual(result[4], 2) #Durée par défaut
    
    def test_isparsable(self):
        """
        Test pour la fonction isparsable
        """
        self.assertFalse(isparsable("...."))
        self.assertTrue(isparsable("12"))
        self.assertFalse(isparsable("benji_la_menace"))
        self.assertTrue(isparsable(".6"))
    
    def test_retrieve_index_carre(self):
        """
        Test pour la fonction permettant de retourver l'index du carré dans tab_forest suite à un clic
        """
        param = 20
        ##Premier carré avec ces paramètres
        self.assertEqual([0, 0], retrieve_index_carre(19, 19, param))
        self.assertEqual([0, 0], retrieve_index_carre(1, 1, param))
        param = 25
        self.assertEqual([0, 0], retrieve_index_carre(24, 24, param))

        ##Deuxième carré
        self.assertEqual([0, 1], retrieve_index_carre(19, 30, param))

        ##Troisème
        self.assertEqual([0, 2], retrieve_index_carre(19, 60, param))

        self.assertEqual([1, 1], retrieve_index_carre(30, 30, param))

        self.assertEqual([20, 20], retrieve_index_carre(502, 502, param))

        ## -1 -1 valeur par défaut pour signaler que le clic n'est pas valide
        self.assertEqual([-1, -1], retrieve_index_carre(25, 25, param)) ##Car ce clic tombe sur un croisement entre 4 carré

    def test_still_fire(self):
        """
        Test de la méthode still_fire
        """
        new_list = list()
        #On initialise un tableau avec 0 feu
        for index_a in range(10):
            other_list = list()
            for index_b in range(10):
                other_list.append("vide")
            new_list.append(other_list)

        self.assertFalse(still_fire(new_list))

        new_list[5][5] = "feu"

        self.assertTrue(still_fire(new_list))

    def test_still_cinder(self):
        """
        Test de la méthode still_cinder
        """
        new_list = list()
        #On initialise un tableau avec 0 cendre
        for index_a in range(10):
            other_list = list()
            for index_b in range(10):
                other_list.append("vide")
            new_list.append(other_list)

        self.assertFalse(still_cinder(new_list))

        new_list[5][5] = "cendre"

        self.assertTrue(still_cinder(new_list))